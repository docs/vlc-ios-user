.. _audio:

#####
Audio
#####

VLC for iOS can be used to play several audio formats such as .asf, .avi, .divx, .dv, .mxf, .ogg, .gm, .ps, .ts, .vob, 
and .wmv.

.. figure::  /images/basic/audio/audio_homepage.png
   :align:   center

\

When a new audio track is added, VLC checks its metadata to extract and organize it correctly. 

.. figure::  /images/basic/audio/album_explanation.png
   :align:   center

\

The audio files are organized based on the artist's name, the audio file's album, the song's name, and genre. 

.. figure::  /images/basic/audio/audio_tabs.png
   :align:   center

\

Adding audio files to VLC
*************************

To add an audio file to VLC for iOS, use any of the :doc:`/gettingstarted/media_synchronization` methods. 
  
Sorting audio files
*******************

By default, VLC sorts audio files in alphanumerical order. However, you can change the sorting format into any of the following order:

* Descending Order.
* Album.
* Duration.
* Release date.

To sort audio files:

* On devices running with iOS versions **below than 13.0**, tap on the :guilabel:`Sorting` icon and select the sorting order. 

.. figure::  /images/basic/audio/sorting_audio_files.png
   :align:   center
   :width: 40%

\

* On devices running with iOS versions **above than 13.0**, tap on the :guilabel:`Menu` icon and select the :guilabel:`Sort by` menu.

.. figure::  /images/basic/audio/sorting_audio_files_menu.png
   :align:   center
   :width: 40%

\

The descending order is defined by an arrow. When the arrow is up, the order is ascending, when the arrow is down it means the descending order is applied.
To switch between the orders, select the current sorting criteria.

Sharing audio files
*******************

Sharing audio files can be achieved seamlessly on VLC for iOS. Follow the steps listed below to transfer audio files with your friends and family. 

1. Enter the :guilabel:`Edit` mode by:
    * Tapping on the :guilabel:`Edit` icon at the top-right corner of your screen on devices running with iOS versions **below than 13.0**.
    * Tapping on the :guilabel:`Menu` icon at the top-right corner of your screen and the :guilabel:`Select` button, on devices running with iOS versions **above than 13.0**.
2. Select the audio files or albums you want to share. 
3. Tap on the :guilabel:`Share` icon and select your preferred sharing platform. 
4. Tap on :guilabel:`Done` to complete the file sharing process. 

Renaming audio files
********************

If you ever want to rename an audio file, follow these steps.

1. Enter the :guilabel:`Edit` mode by:
    * Tapping on the :guilabel:`Edit` icon at the top-right corner of your screen on devices running with iOS versions **below than 13.0**.
    * Tapping on the :guilabel:`Menu` icon at the top-right corner of your screen and the :guilabel:`Select` button, on devices running with iOS versions **above than 13.0**.
2. Tap on the :guilabel:`Edit` icon at the top-right corner of your screen.
3. Select the audio files you want to rename. 
4. Tap on the :guilabel:`Rename` icon. 
5. Change the audio file name and tap on :guilabel:`Rename` to complete the renaming process. 

Deleting audio files
*********************

Follow the steps below to delete audio files on VLC.

1. Enter the :guilabel:`Edit` mode by:
    * Tapping on the :guilabel:`Edit` icon at the top-right corner of your screen on devices running with iOS versions **below than 13.0**.
    * Tapping on the :guilabel:`Menu` icon at the top-right corner of your screen and the :guilabel:`Select` button, on devices running with iOS versions **above than 13.0**.
2. Select the audio files you want to delete. 
3. Tap on the :guilabel:`Delete` icon. 
4. On the pop-up modal on your screen, tap on :guilabel:`Delete` to confirm the selection's deletion.
