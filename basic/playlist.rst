.. _playlist:

########
Playlist
########

A playlist is a list of video or audio files that can be played back on the media player either sequentially or in a shuffled order. In this section, you will learn how to add video or audio file (s) to a playlist on VLC for iOS. 

How to add audio files to a playlist
**************************************

To add audio files to a playlist on VLC for iOS, follow the steps below:

1. Open your VLC for iOS application, and tap on :guilabel:`Audio` icon at the bottom-left corner of your iPhone.

.. figure::  /images/basic/playlist/audio.jpeg
   :align:   center
   :width: 60%

\

2. On the :guilabel:`Audio` pane, enter the :guilabel:`Edit` mode by:
    * Tapping the :guilabel:`Edit` icon at the top-right corner of your screen, on devices running with iOS versions **below than 13.0**.

.. figure::  /images/basic/playlist/edit_icon_audio.jpeg
   :align:   center
   :width: 40%

\

    * Tapping the :guilabel:`Menu` icon at the top-right of your screen and the :guilabel:`Select` button, on devices running with iOS versions **above than 13.0**.

.. figure::  /images/basic/playlist/edit_icon_audio_menu.png
   :align:   center
   :width: 40%

\

3. Select the audio file(s) you want to add to the playlist and tap on :guilabel:`Add to Playlist`. 

.. figure::  /images/basic/playlist/select_audio_files.png
   :align:   center
   :width: 40%

\

4. You can either select an already existing playlist to append your medias to, or create a new playlist.
Here we choose to create a new one.

.. figure::  /images/basic/playlist/create_audio_playlist.png
   :align:   center
   :width: 40%

\

5. Enter a title for your playlist and tap on :guilabel:`Done`.

.. figure::  /images/basic/playlist/playlist_title_audio.png
   :align:   center
   :width: 40%

\

6. To see your newly created playlist, tap on the :guilabel:`Playlist` icon.

.. figure::  /images/basic/playlist/audio_playlist.png
   :align:   center
   :width: 40%

\

How to add videos to a playlist
********************************

To add videos to a playlist on VLC for iOS, follow the steps below:

1. Open your VLC for iOS application, and tap on :guilabel:`Video` icon at the bottom-left corner of your iPhone.

.. figure::  /images/basic/playlist/video.jpeg
   :align:   center
   :width: 60%

\

2. On the :guilabel:`Video` pane, enter the :guilabel:`Edit` mode by:
    * Tapping on the :guilabel:`Edit` icon at the top-right corner of your screen, on devices running with iOS versions **below than 13.0**.

.. figure::  /images/basic/playlist/edit_icon_video.jpeg
   :align:   center
   :width: 40%

\

    * Tapping on the :guilabel:`Menu` icon at the top-right corner of your screen and the :guilabel:`Select` button, on devices running with iOS versions **above than 13.0**.

.. figure::  /images/basic/playlist/edit_icon_video_menu.png
   :align:   center
   :width: 40%

\

3. Select the video(s) you want to add to the playlist, and tap on :guilabel:`Add to Playlist`. 

.. figure::  /images/basic/playlist/select_videos.png
   :align:   center
   :width: 40%

\

4. You can either select an already existing playlist to append your medias to, or create a new playlist. Here we choose to create a new one.

.. figure::  /images/basic/playlist/create_video_playlist.png
   :align:   center
   :width: 40%

\

5. Enter a title for your playlist and tap on :guilabel:`Done`.

.. figure::  /images/basic/playlist/playlist_title_video.png
   :align:   center
   :width: 40%

\

6. To see your newly created playlist, tap on the :guilabel:`Playlist` icon.

.. figure::  /images/basic/playlist/video_playlist.png
   :align:   center
   :width: 40%
