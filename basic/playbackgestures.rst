.. _playbackgestures:

#################
Playback Gestures
#################

In addition to the buttons on VLC,  you can also use different gestures to control the playback of media files. 

Here’s a list of some interesting things you can do:

1. Use your two fingers to tap anywhere on the playback screen to **pause** or **play** the current media. 
2. Pinch your screen with two fingers to **minimize** the current media.
3. Swipe down at the top of the screen in order to **minimize** the player.
4. Swipe to the left to **rewind** the current media by 10 seconds.
5. Swipe to the right to **fast forward** the current media by 30 seconds.
6. Change the current **volume** by swiping vertically on the right-hand side of the playback view.
7. Adapt **screen brightness** by swiping vertically on the left-hand side of the playback view.
8. Double tap on the side to **seek/prev** 10 seconds.
9. A long tap on a media group will display all the **Edit** icons.
