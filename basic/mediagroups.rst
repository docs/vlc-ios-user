.. _mediagroups:

############
Media Groups
############

A media group is a set of media files grouped automatically or manually inside a folder in the video tab of VLC.

.. figure::  /images/basic/mediagroups/media_group.png
   :align:   center
   :width: 40%

\

The highlighted section in the screenshot above is an example of a media group in VLC. In this case, the media group's name will be named 'Sample Videos'. This group has three media files.

*****************************************
How to create a media group automatically
*****************************************

VLC automatically creates a media group for media files with a similar name. This automatic detection and grouping is only 
done based on the similarity of the media file's name. 

For instance, ``Hey123.mp4`` and ``Hey1234.mp4`` will be grouped into the same media group named ``Hey123``. However, ``123hey.mp4`` and ``Hey123.mp4`` will not be grouped into the same media group automatically because the beginning of the media file names is different. 

.. figure::  /images/basic/mediagroups/automatic_media_groups.png
   :align:   center
   :width: 60%

\

Now that you understand how VLC groups media files automatically, follow the steps below to try it out yourself. 

1. First, open VLC for iOS.
2. Use the Wi-Fi Upload or any other :doc:`/gettingstarted/media_synchronization` method of your choice to sync/download media files into your VLC app.
3. For this to work, ensure that the media files you want VLC to group automatically follow the naming format explained above.
4. After moving the media files to VLC, tap on the :guilabel:`Video` icon at the bottom-left corner of your screen. If you followed the steps above, you should see a new media group containing the videos you added to VLC. 
 
************************************
How to create a media group manually
************************************

To create a media group manually, follow the steps below:

1. Open VLC for iOS application on your mobile phone.
2. Tap on the :guilabel:`Video` icon at the bottom-left corner of your screen.
3. Ensure you already have media files on VLC because that's what we intend to group manually.
4. Enter the :guilabel:`Edit` mode:
    * On devices running with iOS versions **below than 13.0**, tap on the :guilabel:`Edit` icon located at the top-right corner of your screen.
    * On devices running with iOS versions **equal to or above 13.0**, tap on the :guilabel:`Menu` icon at the top-right corner of your screen and tap on the :guilabel:`Select` icon.
5. Select the videos you want to add to the media group and tap on the :guilabel:`Add to media group` icon.
6. Tap on the :guilabel:`New media group` at the bottom of your screen, and enter a preferred name for the media group.
7. Tap on :guilabel:`Done` to complete the media group creation process. 

If you followed the steps above, you should see the newly created group in your VLC for iOS application. 

********************
Sorting media groups
********************

Media groups are sorted in alphanumerical order by default. However, you can change the sorting format into any of the following order:

* Descending Order.
* Duration.
* Insertion date.
* Last modification date.
* Number of videos.

To sort a media group:

* On devices running with iOS versions **below than 13.0** tap on the :guilabel:`Sorting` icon and select the sorting order.

.. figure::  /images/basic/mediagroups/sorting_media_groups.png
   :align:   center
   :width: 40%

\

* On devices running with iOS versions **above 13.0** tap on the :guilabel:`Menu` icon and select the :guilabel:`Sort by` menu.

.. figure::  /images/basic/mediagroups/sorting_media_groups_menu.png
   :align:   center
   :width: 40%

\

The descending order is defined by an arrow. When the arrow is up, the order is ascending, when the arrow is down it means the descending order is applied.
To switch between the orders, select the current sorting criteria.

********************
Sharing media groups
********************

You can share media groups with anyone. Follow the steps below to share a media group.

1. Enter the :guilabel:`Edit` mode by:
    * Tapping on the :guilabel:`Edit` icon at the top-right corner of your screen on devices running with iOS versions **below than 13.0**.
    * Tapping on the :guilabel:`Menu` icon at the top-right corner of your screen and the :guilabel:`Select` button, on devices running with iOS versions **above than 13.0**.
2. Select the media group you want to share. 
3. Tap on the :guilabel:`Share` icon and select your preferred sharing platform. 
4. Tap on :guilabel:`Done` to complete the media group sharing process. 

*********************
Renaming media groups 
*********************

If you ever want to rename a media group, follow these steps.

1. Enter the :guilabel:`Edit` mode by:
    * Tapping on the :guilabel:`Edit` icon at the top-right corner of your screen on devices running with iOS versions **below than 13.0**.
    * Tapping on the :guilabel:`Menu` icon at the top-right corner of your screen and the :guilabel:`Select` button, on devices running with iOS versions **above than 13.0**.
2. Select the media group you want to rename.
3. Tap on the :guilabel:`Rename` icon. 
4. Change the media group's name and tap on :guilabel:`Rename` to complete the renaming process. 

*********************
Deleting media groups
*********************

Follow the steps below to delete a media group.

1. Enter the :guilabel:`Edit` mode by:
    * Tapping on the :guilabel:`Edit` icon at the top-right corner of your screen on devices running with iOS versions **below than 13.0**.
    * Tapping on the :guilabel:`Menu` icon at the top-right corner of your screen and the :guilabel:`Select` button, on devices running with iOS versions **above than 13.0**.
2. Select the media file you want to delete. 
3. Tap on the :guilabel:`Delete` icon. 
4. On the pop-up on your screen, tap on :guilabel:`Delete`.

************************************************
Other things you need to know about media groups
************************************************

This section covers other essential things you need to know about media groups. 

1. You can't have an empty media group. So if you delete all the media files in an existing media group, VLC will delete that media group automatically.
2. A long tap on a media group will display all the :guilabel:`Edit` icons. With this, you can easily make changes to your media groups.

.. figure::  /images/basic/mediagroups/media_group_long_tap.png
   :align:   center
   :width: 40%

\

3. Media groups can be regrouped in alphabetical order. To do this, enter the :guilabel:`Edit` mode by:
    * Tapping on the :guilabel:`Edit` icon at the top-right corner of your screen on devices running with iOS versions **below than 13.0**.
    * Tapping on the :guilabel:`Menu` icon at the top-right corner of your screen and the :guilabel:`Select` button, on devices running with iOS versions **above than 13.0**.
4. Tap on the :guilabel:`Regroup` icon. Then click on :guilabel:`Okay` to complete the regrouping process.

.. container:: tocdescr

   .. container:: descr

      .. figure:: /images/basic/mediagroups/edit_icon.png
         :align:  center

   .. container:: descr

      .. figure:: /images/basic/mediagroups/regroup_icon.png
         :align:  center

\

.. container:: tocdescr

   .. container:: descr

      .. figure:: /images/basic/mediagroups/edit_icon_menu.png
         :align:  center
         :width: 80%

   .. container:: descr

      .. figure:: /images/basic/mediagroups/regroup_icon_menu.png
         :align:  center
         :width: 80%
