.. _report-a-bug:

###################
How to Report a Bug
###################

This section lists down guidelines for getting support for any spotted error in VLC for iOS.

*****************************
Are you sure it's a true bug?
*****************************

Well, you know the old adage: "Have you tried turning it off and on again?". Don't be mistaken - we believe you but sometimes, it is truly not our fault. 
So, before concluding that what you are experiencing is a bug, try these steps below:

* Restart your iOS device and relaunch VLC.
* If you are still experiencing the same issues, try to uninstall and re-install VLC.

If none of the aforementioned suggestions worked, then it is really a bug! Do go ahead and report a bug following the steps listed below.


**********************
Is it truly a new bug?
**********************

A large proportion of the submitted bug reports are not really bugs at all, or have been known for months, and/or are already fixed. To avoid that, please check the following:

1. Confirm if you are using the latest version. Bug reports on older versions of VLC for iOS are likely to be ignored, because changes in newer versions of VLC may have already fixed the issue.
2. Search the `list of known and fixed issues <https://code.videolan.org/videolan/vlc-ios/-/issues>`__ to confirm if someone else has not already filed the bug you want to file now. If you find your issue, see if it is still open or has already closed. If it is still open, drop a message remindnig us that you are still experiencing the same issue. But, it has been closed, then it means we've shipped a fix for it.

Once you've confirmed that it isn't a new bug and has not been filed by any other person. Follow the steps below to report the bug.

***************
Reporting a bug
***************

To report a bug, go to the `VLC for iOS repository <https://code.videolan.org/videolan/vlc-ios>`__ and create an `issue <https://code.videolan.org/videolan/vlc-ios>`__ describing the problem you are facing. Our team will review it and fix the bug.