.. _ftp:

###
FTP
###

FTP (File Transfer Protocol) is a standard network protocol used to transfer files between a client and server on a network. With VLC for iOS, you can stream media files from any FTP server seamlessly as long as your iPhone, and the FTP server is on the same network. 

This documentation covers how to set up an FTP server on a Windows computer and stream media (audio and video) files from that server directly on VLC for iOS. 

**************************************
How to set up an FTP server on Windows
**************************************

We will be using a free FTP solution called `FileZilla <https://filezilla-project.org/download.php?type=server>`_ to set up an FTP server on a Windows computer in this documentation. 

Follow the steps below to set up an FTP server on a Windows computer. 

1. Download `FileZilla <https://filezilla-project.org/download.php?type=server>`_ and install it on your local machine. 
2. Once you open FileZilla on your local machine, a popup modal similar to the image below will be displayed on your screen. When you see this, click on :guilabel:`Connect`. 

.. figure::  /images/advanced/networkshares/ftp/ftp_home.PNG
   :align:   center

3. After connecting to the FTP solution, you will be required to add a **User Account**. To do this, click on :menuselection:`Edit --> Users`.

.. figure::  /images/advanced/networkshares/ftp/add_user_account.png
   :align:   center

4. On the **Users** pane, click on :guilabel:`Add`.

.. figure::  /images/advanced/networkshares/ftp/users_pane.PNG
   :align:   center

5. Enter the name of the user account and click :guilabel:`OK`.

.. figure::  /images/advanced/networkshares/ftp/add_user_details.PNG
   :align:   center

6. Add a password for the user account and click on :guilabel:`OK`.

.. figure::  /images/advanced/networkshares/ftp/add_password.PNG
   :align:   center

7. After you click on :guilabel:`Ok`, a popup modal with the message "You need to share at least one directory and set it as home directory". Click on :guilabel:`OK` when you see this message to add a **directory** for the newly created user account.
8. On the Users pane, click on the :guilabel:`Add` button as seen in the image below, select your preferred directory and click :guilabel:`OK`. 

.. container:: tocdescr

   .. container:: descr

      .. figure::  /images/advanced/networkshares/ftp/directory.PNG

   .. container:: descr

      .. figure::  /images/advanced/networkshares/ftp/choose_directory.PNG


9. Finally, click on :guilabel:`OK` to complete the user account creation. 

.. note:: If you encounter any errors regarding your local or external network, it's because of your computer's firewall settings. You can fix this issue by following the steps below:

1. Search for **Windows Defender Firewall** on your computer. 
2. Click on :guilabel:`Allow app or feature through Windows Defender Firewall`. 

.. figure::  /images/advanced/networkshares/ftp/windows_defender_firewall.PNG
   :align:   center

3. Click on :menuselection:`Change Settings --> Allow another app...`.

.. figure::  /images/advanced/networkshares/ftp/change_settings.PNG
   :align:   center

4. Click on :guilabel:`Browse`, navigate to the FileZilla's path and click on :menuselection:`FileZilla Server --> Open.`

.. figure::  /images/advanced/networkshares/ftp/add_app_path.PNG
   :align:   center

5. Finally, click on :guilabel:`Add`. 

.. figure::  /images/advanced/networkshares/ftp/complete_add_a_app.PNG
   :align:   center

********************************************
How to stream media files from an FTP server
********************************************

After setting up an FTP server, you can now stream media (video and audio) directly from VLC. Here's how to stream media files from an FTP file server protocol:

1. Open VLC on your iPhone and tap on :guilabel:`Network`.

.. figure::  /images/advanced/networkshares/ftp/network_tab.jpeg
   :align:   center
   :width: 30%

2. Tap on :guilabel:`Connect` at the top right-corner of your mobile screen.
3. On the connect pane, tap on :guilabel:`FTP`.
4. Add the FTP server's IP address, port, name of the user account, and password. *To find the IP address of your computer, go to your terminal and type ipconfig. Your port will be displayed when you open FileZilla on your computer.* 

.. figure::  /images/advanced/networkshares/ftp/connect_to_ftp.jpeg
   :align:   center
   :width: 30%


5. Finally, tap on :guilabel:`Connect` to access the media files on the FTP server. 

If you followed the steps above correctly, all the media files you have access to on the FTP server would now be visible on VLC for iOS. Tap on your preferred media file and enjoy streaming.

.. figure::  /images/advanced/networkshares/ftp/stream_media.jpeg
   :align:   center
   :width: 30%
