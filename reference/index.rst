##################
 Reference Manual
##################

This section will describe (almost) exhaustively all the features and modules of VLC for iOS.

.. toctree::
   :maxdepth: 2

   os_compatibility.rst
